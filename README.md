# SELENIUM SCREENSHOT

## Description

Tool to get full or element screenshots with Selenium and headless Firefox.

## Install

Get last release of *geckodriver* here : <https://github.com/mozilla/geckodriver/releases>  
Unpack it in /usr/bin or create symlink in order to add *geckodriver* in Python path.

```bash
$ sudo tar xf ~/path/to/geckodriver-release.tar.gz --directory=/usr/bin
```

Create virtual environment: 
```bash
$ cd /path/to/screenshot
$ python3 -m venv venv
$ source venv/bin/activate
```

Install pip packages: 

``` bash
$ pip install -r requirements.txt
```

## Settings

If you want to select webpage element, set x_path as following: 

```python
x_path = "//div[@class='fig-list-articles']"
```

You can get `XPath` from the browser developer tools.  

## Usage

Use `main.py`